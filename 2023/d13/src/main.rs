use std::fs;

fn main() {

    let data: Vec<String> = fs::read_to_string("example").unwrap().lines().map(String::from).collect();

    part1(&data);
    part2(&data);
}


fn part1(data: &Vec<String>) {
    let mut patterns: Vec<Vec<String>> = Vec::new();
    let mut data2 = data.clone();

    data2.reverse();

    let mut pattern: Vec<String> = Vec::new();

    while data2.len() > 0  {
        let temp = data2.pop().unwrap();

        if temp.len() == 0 {
            patterns.push(pattern);
            pattern = Vec::new();
        }
        else {
            pattern.push(temp);
        }
    }

    patterns.push(pattern);
    
    let mut sum = 0;

    for pattern in &patterns {
        for p in pattern {
            println!("{p}")
        }
        println!("");

        let mut split_index = find_horizontal(pattern);

        if split_index != pattern.len() {
            sum += 100 * (split_index + 1);
        }

        split_index = find_vertical(pattern);

        if split_index != pattern[0].len() {
            sum += split_index + 1;
        }
    }

    println!("Part 1: {}", sum);
}


fn part2(data: &Vec<String>) {
    let mut patterns: Vec<Vec<String>> = Vec::new();
    let mut data2 = data.clone();
    let mut part1_indexes: Vec<usize> = Vec::new();

    data2.reverse();

    let mut pattern: Vec<String> = Vec::new();

    while data2.len() > 0  {
        let temp = data2.pop().unwrap();

        if temp.len() == 0 {
            patterns.push(pattern);
            pattern = Vec::new();
        }
        else {
            pattern.push(temp);
        }
    }

    patterns.push(pattern);
    
    let mut sum = 0;

    for pattern in &patterns {
        for p in pattern {
            println!("{p}")
        }
        println!("");

        let mut split_index = find_horizontal(pattern);

        if split_index != pattern.len() {
            sum += 100 * (split_index + 1);
            part1_indexes.push(split_index);
        }

        split_index = find_vertical(pattern);

        if split_index != pattern[0].len() {
            sum += split_index + 1;
            part1_indexes.push(split_index);
        }
    }


    for i in 0..patterns.len() {



        //TODO




        let mut split_index = find_horizontal(&patterns[i]);

        if split_index != part1_indexes[i] && split_index != patterns[i].len() {
            sum += 100 * (split_index + 1);
        }

        split_index = find_vertical(&patterns[i]);


        if split_index != part1_indexes[i] && split_index != patterns[i][0].len() {
            sum += split_index + 1;
        }
    }



    println!("Part 2: {}", sum);
}


fn find_horizontal(pattern: &Vec<String>) -> usize {
    let mut split_index = pattern.len();

    for i in 0..pattern.len() - 1 {
        let mut pattern_found = false;

        if pattern[i] == pattern[i + 1] {
            for j in 1..pattern.len() {
                pattern_found = true;

                if (i as i32 - j as i32) < 0 || i + 1 + j >= pattern.len() {
                    break;
                }

                if pattern[i - j] != pattern[i + 1 + j] {
                    pattern_found = false;
                    break;
                }
            }

            if pattern_found {
                split_index = i;
                break;
            }
        }
    }

    split_index
}


fn find_vertical(pattern: &Vec<String>) -> usize {
    let mut split_index = pattern[0].len();

    for i in 0..pattern[0].len() - 1 {
        let mut pattern_found = true;

        for j in 0..pattern.len() {
            if pattern[j][i..=i] != pattern[j][i + 1..=i + 1] {
                pattern_found = false;
                break;
            }
        }

        if pattern_found {
            'cat: for j in 1..pattern[0].len() {
                if (i as i32 - j as i32) < 0 || i + 1 + j >= pattern[0].len() {
                    break;
                }

                for k in 0..pattern.len() {
                    if pattern[k][i - j..=i - j] != pattern[k][i + 1 + j..=i + 1 + j] {
                        pattern_found = false;
                        break 'cat;
                    }
                }
            }
        }

        if pattern_found {
            split_index = i;
        }
    }

    split_index
}
