use std::fs;
use std::collections::HashMap;

fn main() {

    let data: Vec<String> = fs::read_to_string("input").unwrap().lines().map(String::from).collect();

    part1n2(&data);
}


fn part1n2(data: &Vec<String>) {
    let mut springs: Vec<(Vec<char>, Vec<usize>)> = Vec::new();

    for d in data {
        let temp: Vec<&str> = d.split(" ").collect();
        let spring: Vec<char> = temp[0].chars().collect();
        let counts: Vec<usize> = temp[1].split(",").map(|s| s.parse::<usize>().unwrap()).collect();

        springs.push((spring, counts));
    }

    let mut sum = 0;

    for s in &springs {
        let mut cache: HashMap<(usize, usize, i64), i64> = HashMap::new();

        sum += solve(0, 0, 0, &s.0, &s.1, &mut cache);
    }

    println!("Part 1: {}", sum);

    sum = 0;

    for s in &springs {
        let mut new_spring: Vec<char> = s.0.clone();
        let mut new_counts: Vec<usize> = s.1.clone();
        let mut cache: HashMap<(usize, usize, i64), i64> = HashMap::new();

        for _i in 0..4 {
            new_spring.push('?');
            new_spring.extend(s.0.iter());
            new_counts.extend(s.1.iter());
        }

        sum += solve(0, 0, 0, &new_spring, &new_counts, &mut cache);
    }

    println!("Part 2: {}", sum);
}


fn solve(spring_index: usize, count_index: usize, current: i64, springs: &Vec<char>, counts: &Vec<usize>, cache: &mut HashMap<(usize, usize, i64), i64>) -> i64 {
    if cache.contains_key(&(spring_index, count_index, current)) {
        return *cache.get(&(spring_index, count_index, current)).unwrap();
    }

    let mut sum = 0;

    if spring_index == springs.len() {
        if count_index < counts.len() {
            return 0;
        }

        return 1;
    }

    if count_index == counts.len() {
        for i in spring_index..springs.len() {
            if springs[i] == '#' {
                return 0;
            }
        }

        return 1;
    }

    match springs[spring_index] {
        '?' => {
            if current == -1 {
                sum += solve(spring_index + 1, count_index, 0, springs, counts, cache);
            }
            else if current == 0 {
                sum += solve(spring_index + 1, count_index, 0, springs, counts, cache);

                if current + 1 == counts[count_index] as i64 {
                    sum += solve(spring_index + 1, count_index + 1, -1, springs, counts, cache);
                }
                else {
                    sum += solve(spring_index + 1, count_index, 1, springs, counts, cache);
                }
            }
            else if current > 0 {
                if current + 1 == counts[count_index] as i64 {
                    sum += solve(spring_index + 1, count_index + 1, -1, springs, counts, cache);
                }
                else if current + 1 < counts[count_index] as i64 {
                    sum += solve(spring_index + 1, count_index, current + 1, springs, counts, cache);
                }
            }

        },
        '.' => {
            if current <= 0 {
                sum += solve(spring_index + 1, count_index, 0, springs, counts, cache);
            }
        },
        '#' => {
            if !(current < 0 || current + 1 > counts[count_index] as i64) {
                if current + 1 == counts[count_index] as i64 {
                    sum += solve(spring_index + 1, count_index + 1, -1, springs, counts, cache);
                }
                else {
                    sum += solve(spring_index + 1, count_index, current + 1, springs, counts, cache);
                }
            }
        },
        _ => {}
    }

    cache.insert((spring_index, count_index, current), sum);

    sum
}
