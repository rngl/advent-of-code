﻿namespace d17;

using System.IO;
using System.Security.Cryptography;

class Program
{
    static void Main(string[] args)
    {
        string file = "";

        if (args.Length > 0)
        {
            file = args[0];
        }
        
        string data;

        using (StreamReader sr = new(file))
        {
            data = sr.ReadToEnd();
        };

        Part1n2(data);
    }


    static void Part1n2(string data)
    {
        string[] data_lines = data.Split("\n", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        foreach (string line in data_lines)
        {
            Console.WriteLine(line);

            State start = new(0, 0, "");
            List<State> states = Move(start, line);
            State? shortest = null;
            State? longest = null;

            while (states.Count > 0)
            {
                List<State> new_states = [];

                foreach (State s in states)
                {
                    if (s.x != 3 || s.y != 3)
                        new_states.AddRange(Move(s, line));
                }

                State? exit = FoundExit(new_states);

                if (exit is not null)
                {
                    shortest ??= exit;
                    longest = exit;
                }

                states = new_states;
            }

            if (shortest.HasValue)
                Console.WriteLine("Part 1: {0}", shortest.Value.path);
            if (longest.HasValue)
                Console.WriteLine("Part 2: {0}", longest.Value.path.Length);
        }
    }


    static State? FoundExit(List<State> states)
    {
        foreach (State state in states)
        {
            if (state.x == 3 && state.y == 3)
                return state;
        }

        return null;
    }


    static List<State> Move(State start, string passcode)
    {
        HashSet<char> open = ['b', 'c', 'd', 'e', 'f'];
        List<State> new_states = [];

        string code = passcode + start.path;
        Byte[] code_bytes = System.Text.Encoding.ASCII.GetBytes(code);
        string hash = Convert.ToHexString(MD5.HashData(code_bytes)).ToLower();

        for (int i = 0; i < 4; i++)
        {
            if (open.Contains(hash[i])) 
            {
                if (i == 0 && start.y > 0)
                    new_states.Add(new State(start.x, start.y - 1, start.path + "U"));
                if (i == 1 && start.y < 3)
                    new_states.Add(new State(start.x, start.y + 1, start.path + "D"));
                if (i == 2 && start.x > 0)
                    new_states.Add(new State(start.x - 1, start.y, start.path + "L"));
                if (i == 3 && start.x < 3)
                    new_states.Add(new State(start.x + 1, start.y, start.path + "R"));
            }
        }

        return new_states;
    }


}
 public struct State(int x, int y, string path)
{
    public int x = x;
    public int y = y;
    public string path = path;
}

