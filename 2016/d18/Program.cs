﻿namespace d18;

using System.IO;

class Program
{
    static void Main(string[] args)
    {
        string file = "";
        int row_count = 10;

        if (args.Length > 0)
        {
            file = args[0];
            if (file == "input")
                row_count = 40;
        }
        
        string data;

        using (StreamReader sr = new(file))
        {
            data = sr.ReadToEnd();
        };

        Part1n2(data, row_count, 1);
        Part1n2(data, 400000, 2);
    }


    static void Part1n2(string data, int row_count, int part)
    {
        string[] data_lines = data.Split("\n", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        foreach (string line in data_lines)
        {
            int safe_tiles = 0;
            List<string> rows = [];
            rows.Add(line);

            for (int i = 0; i < row_count - 1; i++)
            {
                rows.Add(NextRow(rows[^1]));
            }

            foreach (string row in rows) {
                if (row_count < 100)
                    Console.WriteLine(row);
                safe_tiles += row.Count(c => c == '.');
            }
                
            Console.WriteLine("Part {0}: {1}", part, safe_tiles);
        }
    }


    static string NextRow(string row)
    {
        string new_row = "";

        for (int i = 0; i < row.Length; i++)
        {
            bool left = false;
            bool right = false;
            bool center = false;

            if (i - 1 >= 0)
                if (row[i - 1] == '^')
                    left = true;
            if (row[i] == '^')
                center = true;
            if (i + 1 < row.Length)
                if (row[i + 1] == '^')
                    right = true;

            if ((left && center && !right) || 
                (!left && center && right) ||
                (left && !center && !right) ||
                (!left && !center && right))
                new_row += '^';
            else
                new_row += '.';
        }

        return new_row;
    }

}
