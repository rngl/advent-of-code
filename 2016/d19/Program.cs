﻿namespace d19;

using System.IO;

class Program
{
    static void Main(string[] args)
    {
        string file = "";

        if (args.Length > 0)
        {
            file = args[0];
        }
        
        string data;

        using (StreamReader sr = new(file))
        {
            data = sr.ReadToEnd();
        };

        Part1(data);
        Part2(data);
    }


    static void Part1(string data)
    {
        string[] data_lines = data.Split("\n", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        foreach (string line in data_lines)
        {
            int count = int.Parse(line);
            Console.WriteLine(count);

            LinkedList<Elf> elves = [];

            for (int i = 1; i <= count; i++)
            {
                elves.AddLast(new Elf(i, 1));
            }

            var current = elves.First;

            while (elves.Count > 1) {
                if (current != null && elves.First != null && elves.Last != null)
                {
                    if (current.Value.presents != 0)
                    {
                        if (current.Next != null) {
                            current.ValueRef.presents += current.Next.Value.presents;
                            current.Next.ValueRef.presents = 0;
                        }
                        else
                        {
                            current.ValueRef.presents += elves.First.Value.presents;
                            elves.First.ValueRef.presents = 0;
                        }
                    }
                    if (current.Previous != null)
                        if (current.Previous.Value.presents == 0) 
                            elves.Remove(current.Previous);
                    else
                    {
                        if (elves.Last.Value.presents == 0)
                            elves.RemoveLast();
                    }

                    if (current.Next == null)
                        current = elves.First;
                    else
                        current = current.Next;
                }
            }

            if (elves.First != null)
                Console.WriteLine("Part 1: {0}", elves.First.Value.id);
        }
    }


static void Part2(string data)
    {
        string[] data_lines = data.Split("\n", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        foreach (string line in data_lines)
        {
            int count = int.Parse(line);
            LinkedList<Elf> elves = [];

            for (int i = 1; i <= count; i++)
            {
                elves.AddLast(new Elf(i, 1));
            }

            var current = elves.First;
            int current_idx = 0;
            var target = elves.First;
            int current_target_idx = 0;

            while (elves.Count > 1) {
                int target_idx = elves.Count / 2 + current_idx;
                
                if (target_idx >= elves.Count)
                    target_idx -= elves.Count;

                while (current_target_idx != target_idx)
                {
                    if (target != null)
                    {
                        if (target.Next != null)
                        {
                            target = target.Next;
                            current_target_idx++;
                        }
                        else
                        {
                            target = elves.First;
                            current_target_idx = 0;
                        }
                    }
                }

                if (current != null && target != null && elves.First != null)
                    {
                    current.ValueRef.presents += target.Value.presents;
                    var to_delete = target;

                    if (target.Next != null)
                    {
                        target = target.Next;
                    }
                    else
                    {
                        target = elves.First;
                        current_target_idx = 0;
                    }
                        
                    elves.Remove(to_delete);

                    if (current.Next == null)
                    {
                        current = elves.First;
                        current_idx = 0;
                    }
                    else
                    {
                        current = current.Next;
                        if (target_idx > current_idx)
                            current_idx++;
                    }
                }
            }
            if (elves.First != null)
                Console.WriteLine("Part 2: {0}", elves.First.Value.id);
        }
    }    
}


public struct Elf(int id, int presents)
{
    public int id = id;
    public int presents = presents;

}